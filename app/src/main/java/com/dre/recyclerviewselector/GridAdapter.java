package com.dre.recyclerviewselector;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {

    private String[] mData;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private View mSelector;

        public ViewHolder(final View view) {
            super(view);

            mTitle = (TextView) view.findViewById(android.R.id.title);
            mSelector = view.findViewById(R.id.selector);
        }
    }

    public GridAdapter(String[] data) {
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_adapter_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTitle.setText(mData[position]);
    }

    @Override
    public int getItemCount() {
        return mData.length;
    }
}